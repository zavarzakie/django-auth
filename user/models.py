from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core.validators import *


class CustomUserManager(BaseUserManager):
    def _create_user(self, username, password, **extra_fields):
        if not username:
            raise ValueError('Username is required')
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('SuperUser must be True')

        if extra_fields.get('is_staff') is not True:
            raise ValueError('is_staff must be True')

        return self._create_user(username, password, **extra_fields)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    GENDER_CHOICES = (
        ('MALE', "male"),
        ('FEMALE', "female"),
    )
    username = models.CharField("Username", max_length=30, unique=True)
    password = models.CharField("Password", max_length=128)
    email = models.EmailField("email", max_length=254, unique=True)
    phone_regex = RegexValidator(regex=r'^09\d{9}',
                                 message="Phone number must be entered in the format: '+999999999'. Up td.")

    phone_number = models.CharField("phone_Number", null=True, blank=True, unique=True,
                                       validators=[phone_regex] , max_length=11)
    first_name = models.CharField("first_Name", max_length=30)
    last_name = models.CharField("last_Name", max_length=30)
    gender = models.CharField("gender", max_length=6, choices=GENDER_CHOICES, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    description = models.TextField("description", null=True, blank=True, max_length=200)
    USERNAME_FIELD = 'username'
    objects = CustomUserManager()

    def __str__(self):
        return self.username


class CodeModel(models.Model):
    email = models.EmailField('email', null=False, blank=False, unique=True, default='a@gmail.com')
    code = models.CharField('code', max_length=20, null=True, blank=True)


    # class Meta:
    #     db_table = 'music_album'
