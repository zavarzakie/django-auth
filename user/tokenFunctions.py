import jwt, datetime
from rest_framework import exceptions
from django.conf import settings
from datetime import datetime, timedelta


def create_token(id, type='accesss'):
    secret_type = settings.SECRET_KEY
    if type == 'refresh':
        secret_type = settings.REFRESH_KEY
    return jwt.encode({
        'id': id,
        'exp': datetime.utcnow() + timedelta(days=1),
        'iat': datetime.utcnow(),

    }, secret_type, algorithm='HS256')


def decode_token(token, type='accesss'):
    try:
        secret_type = settings.SECRET_KEY
        if type == 'refresh':
            secret_type = settings.REFRESH_KEY
        payload = jwt.decode(token, secret_type, algorithms='HS256')
        return payload['id']
    except:
        raise exceptions.AuthenticationFailed('unauthentication')
