# Generated by Django 4.1.3 on 2022-11-16 07:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0008_delete_codemodel'),
    ]

    operations = [
        migrations.CreateModel(
            name='CodeModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(default='a@gmail.com', max_length=254, unique=True, verbose_name='email')),
                ('code', models.CharField(blank=True, max_length=20, null=True, verbose_name='code')),
            ],
            options={
                'db_table': 'music_album',
            },
        ),
    ]
