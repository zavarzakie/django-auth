from rest_framework import status
from .serializer import *
from threading import Timer
from random import randint
from rest_framework.views import APIView
from django.http import *
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from rest_framework.authtoken.views import ObtainAuthToken
from .tokenFunctions import *
from django.conf import settings


def exec_func(query):
    try:
        query.delete()
    except Exception as e:
        print(e)


class Signup(APIView):

    @swagger_auto_schema(
        responses={status.HTTP_201_CREATED: UserSeializer()},
        operation_description="Some description here...",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'username': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
                'password': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
                'email': openapi.Schema(type=openapi.TYPE_STRING, description='string', format=openapi.FORMAT_EMAIL),
                'phone_number': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
                'first_name': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
                'last_name': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
            }
        )
    )
    def post(self, request):
        serializer = UserSeializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return JsonResponse({'result': 'user has created'})

        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Code(APIView):

    @swagger_auto_schema(
        responses={status.HTTP_201_CREATED: CodeSeializer()},
        operation_description="Some description here...",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'email': openapi.Schema(type=openapi.TYPE_STRING, description='string', format=openapi.FORMAT_EMAIL),
            }
        )
    )
    def post(self, request):
        serializers = CodeSerializer(data=request.data)
        if serializers.is_valid():
            code = str(randint(0, 1000000))
            obj, created = CodeModel.objects.update_or_create(email=request.data['email'], code=code)
            message = MIMEMultipart("alternative")
            message["Subject"] = 'کد احراز هویت'
            html = """\
                               <html>
                                 <body>
                                   <p>AThis message has sent from john<br>
                                      <br> your code :
                                       """ + code + """<br/>
                                   </p>
                                 </body>
                               </html>
                               """
            part = MIMEText(html, "html")
            message.attach(part)
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
                server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
                server.sendmail(
                    settings.EMAIL_HOST_USER, request.data['email'], message.as_string()
                )
            try:
                Timer(60, exec_func, [obj]).start()
                return JsonResponse({'result': 'code has sent'}, status=200)
            except:
                return JsonResponse(status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse(serializers.errors)


class Verify(APIView):
    @swagger_auto_schema(
        responses={status.HTTP_201_CREATED: CodeSeializer()},
        operation_description="Some description here...",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'email': openapi.Schema(type=openapi.TYPE_STRING, description='string', format=openapi.FORMAT_EMAIL),
                'code': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
            }
        )
    )
    def post(self, request):
        try:
            query = CodeModel.objects.get(email=request.data['email'], code=request.data['code'])
            query.delete()
            return JsonResponse({'result': 'user confirmed'})
        except Exception as e:
            return JsonResponse({'error': 'user not found'})


class SignIn(ObtainAuthToken):

    @swagger_auto_schema(
        # responses={status.HTTP_201_CREATED: UserSeializer()},
        operation_description="Some description here...",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'username': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
                'password': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
            }
        )
    )
    def post(self, request):
        serializers = self.serializer_class(data=request.data, context={'request': request})
        if serializers.is_valid(raise_exception=True):
            user = serializers.validated_data['user']
            return JsonResponse({
                'token': create_token(user.pk),
                'refresh': create_token(user.pk, 'refresh')})

        return JsonResponse(serializers.errors)


class Refresh(APIView):

    @swagger_auto_schema(
        responses={status.HTTP_201_CREATED: RefreshSerializer()},
        operation_description="Some description here...",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'refresh': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
            }
        )
    )
    def post(self, request):
        serializers = RefreshSerializer(data=request.data)
        if serializers.is_valid():
            id = decode_token(serializers.validated_data['refresh'])
            return JsonResponse({
                'token': create_token(id),
                'refresh': create_token(id, 'refresh')})

        return JsonResponse(serializers.errors)
