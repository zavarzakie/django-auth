
from .models import *

from django.contrib import admin




class CodeAdmin(admin.ModelAdmin):
    list_display = ['id', "email"]
    ordering = ("id",)


admin.site.register(CustomUser)
admin.site.register(CodeModel, CodeAdmin)

# TokenAdmin.raw_id_fields = ['user']
