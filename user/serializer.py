from rest_framework import serializers
from .models import *


class UserSeializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = '__all__'


class CodeSeializer(serializers.ModelSerializer):
    username = serializers.IntegerField(source='customuser.id')

    class Meta:
        model = CodeModel
        fields = ('id', 'code', 'username')


class RefreshSerializer(serializers.Serializer):
    refresh = serializers.CharField(required=True)


class CodeSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)

class SignInSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)