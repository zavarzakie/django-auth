from django.contrib import admin
from django.urls import path

from django.conf.urls.static import static

from user.views import *
from . import settings
from django.contrib import admin
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers
from django.urls import path, include

schema_view = get_schema_view(
    openapi.Info(
        title=" API",
        default_version='v1',
        description="Plan",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('signup/', Signup.as_view()),
    path('code/', Code.as_view()),
    path('verify/', Verify.as_view()),
    path('signin/', SignIn.as_view()),
    path('refreh/', Refresh.as_view()),
]
